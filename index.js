/**
 * Escape special characters in the given string of html.
 *
 * @param  {String} html
 * @return {String}
 */

var request=require("request");

module.exports = {
  getTokenFromPaymentData: function(pubKey, paymentData, success, err){
  
    var token = btoa(pubKey + ":" + "''");
    var form = paymentData;

    var options = {
      method: 'POST',
      uri: 'https://api.paymentspring.com/api/v1/tokens',
      form,
      headers: {
        'Content-Type': 'application/json',
        'Authorization':'Basic ' + token
        },
        json: true 
    };
      request(options, function(error, response, body) {
           if(error){
              err(error);
          }else{
              if(body.id == undefined){
                err(body)
              }
              else{
                success(body);
              }
        }
    });
  },
  retrieveCustomer: function(privKey, customerId, success, err){
    
    var token = btoa(privKey + ":" + "''");

    var options = {
      method: 'GET',
      uri: 'https://api.paymentspring.com/api/v1/customers/' + customerId,
      headers: {
        'Content-Type': 'application/json',
        'Authorization':'Basic ' + token
        },
        json: true 
    };
      request(options, function(error, response, body) {
           if(error){
              err(error);
          }else{
              if(body.id == undefined){
                err(body)
              }
              else{
                success(body);
              }
        }
    });
  },
  chargeCustomer: function(privKey, chargeData, success, err){
    var token = btoa(privKey + ":" + "''");

    var form = chargeData;

    var options = {
      method: 'POST',
      uri: 'https://api.paymentspring.com/api/v1/charge',
      form,
      headers: {
        'Content-Type': 'application/json',
        'Authorization':'Basic ' + token
        },
        json: true
    };
      request(options, function(error, response, body) {
           if(error){
              err(error);
          }else{
              if(body.id == undefined){
                err(body)
              }
              else{
                success(body);
              }
        }
    });
  },
  createCustomerFromToken: function(privKey, customerData, success, err){
    
      var token = btoa(privKey + ":" + "''");

      var form = customerData;

      var options = {
        method: 'POST',
        uri: 'https://api.paymentspring.com/api/v1/customers',
        form,
        headers: {
          'Content-Type': 'application/json',
          'Authorization':'Basic ' + token
          },
          json: true 
      };
        request(options, function(error, response, body) {
             if(error){
                err(error);
            }else{
                if(body.id == undefined){
                  err(body)
                }
                else{
                  success(body);
                }
          }
      });
    },
    createChargeWithData: function(privKey, chargeData, success, err){
        var token = btoa(privKey + ":" + "''");

        var form = chargeData;

        var options = {
          method: 'POST',
          uri: 'https://api.paymentspring.com/api/v1/charge',
          form,
          headers: {
            'Content-Type': 'application/json',
            'Authorization':'Basic ' + token
            },
            json: true
        };
          request(options, function(error, response, body) {
               if(error){
                  err(error);
              }else{
                  if(body.id == undefined){
                    err(body)
                  }
                  else{
                    success(body);
                  }
            }
        });
      }
};